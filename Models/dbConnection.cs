using System;
using MySql.Data.MySqlClient;

namespace HomeService
{
    public class dbConnection
    {
        public MySqlConnection connection{get;set;}
        public dbConnection()
        {
            MyConnection();
        }
        private MySqlConnection MyConnection()
        {
            try
            {
                string GetConnect = ConnectionString.ConnectStr;
                connection = new MySqlConnection(GetConnect);
                if(connection.State == System.Data.ConnectionState.Closed) connection.Open();

            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return connection;
        }
        
        
    }
    public static class ConnectionString
    {
        public static string ConnectStr{get;set;}
    }
}