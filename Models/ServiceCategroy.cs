using System;
using System.Data;
using MySql.Data.MySqlClient;

namespace HomeService
{
    public class ServiceCategroy
    {
        public dbConnection db {get;set;}
        public  MySqlCommand com {get; set;}
        MySqlDataAdapter dap {get; set;}
        DataTable dtable {get; set;}
        public int id { get; set; }
        public string name { get; set; }
        public string slug {get;set;}
        public string imgType { get; set; }
        public string img { get; set; }
        public byte[] images{get;set;}
        public string feather{get;set;}
        //method modify 
        public int Modify(ServiceCategroy service,string query)
        {
            int retResult=0;
            images = Convert.FromBase64String(service.img);
            try
            {
                db = new dbConnection();
                com = new MySqlCommand(query,db.connection);
                //com.CommandType=CommandType.StoredProcedure; if we have store procedure
                com.Parameters.AddWithValue("name",service.name);
                com.Parameters.AddWithValue("slug",service.slug);
                com.Parameters.AddWithValue("imgType",service.imgType);
                com.Parameters.AddWithValue("img",images);
                if(!string.IsNullOrEmpty(service.feather))
                     com.Parameters.AddWithValue("feather",service.feather);
                else
                    com.Parameters.AddWithValue("feather","Yes");
                retResult = com.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                retResult=ex.HResult;
            }
            return retResult;
        }
        public DataTable ShowRows(string query)
        {
            db = new dbConnection();
            dap = new MySqlDataAdapter(query,db.connection);
            dtable = new DataTable();
            dap.Fill(dtable);
            return dtable;
        }
        public int DeleteService(string query,int delId)
        {
            
            int result=0;
            try
            {
                db = new dbConnection();
                com = new MySqlCommand(query,db.connection);
                com.Parameters.AddWithValue("id",delId);
                result = com.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                result = ex.HResult;
            }
            return result;
        }
    }
}