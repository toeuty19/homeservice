using System;
using MySql.Data.MySqlClient;

namespace HomeService
{
    public class service
    {
        public int id { get; set; }
        public string name { get; set; }
        public string slug { get; set; }
        public string tageline{get;set;}
        public int category { get; set; }
        public string categoryName { get; set; }
        public decimal price { get; set; }
        public decimal discount { get; set; }
        public string dis_Type { get; set; }
        public string description { get; set; }
        public string inclusion { get; set; }
        public string exclusion { get; set; }
        public string status {get;set;}
        public string imgType { get; set; }
        public string img { get; set; }
        //create methed
        
        public int ModifyService(service obj,string query)
        {
            dbConnection db = new dbConnection();
            byte[] imgByte=null;
            int result = 0;
            // try
            // {
                MySqlCommand com = new MySqlCommand(query,db.connection);
                if(obj.name!=null && obj.img!=null)
                {
                    com.Parameters.AddWithValue("name",obj.name);
                    com.Parameters.AddWithValue("slug",obj.slug);
                    com.Parameters.AddWithValue("tage",obj.tageline);
                    com.Parameters.AddWithValue("cid",obj.category);
                    com.Parameters.AddWithValue("price",obj.price);
                    com.Parameters.AddWithValue("discount",obj.discount);
                    com.Parameters.AddWithValue("disType",obj.dis_Type);
                    com.Parameters.AddWithValue("des",obj.description);
                    com.Parameters.AddWithValue("inc",obj.inclusion);
                    com.Parameters.AddWithValue("exc",obj.exclusion);
                    com.Parameters.AddWithValue("imgType",obj.imgType);
                    if(obj.img != null)
                        imgByte = Convert.FromBase64String(obj.img);
                    com.Parameters.AddWithValue("img",imgByte);
                }
                else 
                {
                    com.Parameters.AddWithValue("id",obj.id);
                }
                result = com.ExecuteNonQuery();
            //}
            // catch(Exception ex)
            // {
            //     result = ex.HResult;
            // }
            return result;
        }
    }
}