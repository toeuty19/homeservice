namespace HomeService.Models
{
    public class SliderModel
    {
        public int id { get; set; }
        public string title { get; set; }
        public string status { get; set; }
        public string imgType { get; set; }
        public string img { get; set; }
        public string date { get; set; }
        public string lastDate {get;set;}
    }
}