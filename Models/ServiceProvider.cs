namespace HomeService
{
    public class ServiceProvider
    {
        public int id { get; set; } 
        public int serId { get; set; }
        public string name { get; set; }
        public string imgType { get; set; }
        public string img { get; set; }
        public string email {get;set;}
        public string about { get; set; }
        public string phone{get;set;}
        public string city { get; set; }
        public int cateId { get; set; }
        public string cateName{get;set;}
        public string location { get; set; }

    }
}