namespace HomeService
{
    public class UserModel
    {
        public int userid{get;set;}
        public string name{get;set;}
        public string email{get;set;}
        public string phone{get;set;}
        public string password{get;set;}
        public string userType{get;set;}
    }
}