using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using System.Data;

namespace HomeService.Controllers
{
    public class ContactController : Controller
    {
        public IActionResult ViewContact()
        {
            return View();
        }

        private MySqlCommand com {get;set;}
        private dbConnection dbCon {get;set;}
        private MySqlDataAdapter data {get;set;}
        private DataTable table {get;set;}
        private Contacts contacts{get;set;}
        private List<Contacts> listcontact = new List<Contacts>();
        public IActionResult SendMessage(Contacts obj)
        {
            dbCon = new dbConnection();
            com = new MySqlCommand("INSERT INTO `contact`(`name`, `email`, `phone`,message, `sendDate`) VALUES (@name,@email,@phone,@message,@date)",dbCon.connection);
            com.Parameters.AddWithValue("name",obj.name);
            com.Parameters.AddWithValue("email",obj.email);
            com.Parameters.AddWithValue("phone",obj.phone);
            com.Parameters.AddWithValue("message",obj.message);
            com.Parameters.AddWithValue("date",DateTime.Now);
            com.ExecuteNonQuery();
            return Ok(1);
        }
        public IActionResult GetDataContact()
        {
            dbCon = new dbConnection();
            data = new MySqlDataAdapter("SELECT * FROM `contact`",dbCon.connection);
            table = new DataTable();
            data.Fill(table);
            foreach(DataRow row in table.Rows)
            {
                contacts = new Contacts();
                contacts.id = int.Parse(row[0].ToString());
                contacts.name = row[1].ToString();
                contacts.email = row[2].ToString();
                contacts.phone = row[3].ToString();
                contacts.message = row[4].ToString();
                contacts.date = Convert.ToDateTime(row[5].ToString());
                listcontact.Add(contacts);
            }
            return Ok(listcontact);
        }
    }
}