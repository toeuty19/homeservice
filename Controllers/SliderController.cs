using System.Data;
using HomeService.Models;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace HomeService.Controllers
{
    public class SliderController:Controller
    {
        private MySqlCommand com {get;set;}
        private MySqlDataReader dataReader{get;set;}
        private DataTable dataTable{get;set;}
        private dbConnection dbCon{get;set;}
        private byte[] imgbyte;
        private SliderModel obj{get;set;}
        private List<SliderModel> listSlider;
        public IActionResult ViewSlider()
        {
            return View();
        }
        //add new slider
        public IActionResult addSlider(SliderModel getObj)
        {   
            if(getObj.img!=null)
                imgbyte = Convert.FromBase64String(getObj.img);
            dbCon = new dbConnection();
            com = new MySqlCommand("INSERT INTO `slider`(`title`, `status`, `imgType`, `img`, `date`) VALUES (@title,@status,@imgtype,@img,@date)",dbCon.connection);
            com.Parameters.AddWithValue("title",getObj.title);
            com.Parameters.AddWithValue("status",getObj.status);
            com.Parameters.AddWithValue("imgtype",getObj.imgType);
            com.Parameters.AddWithValue("img",imgbyte);
            com.Parameters.AddWithValue("date",DateTime.Now);
            if(com.ExecuteNonQuery()==1) return Ok(1);
            else return Ok(0);
        }
        //select all rows
        public IActionResult getRowFromDatabase()
        {
            dbCon = new dbConnection();
            listSlider = new List<SliderModel>();
            com = new MySqlCommand("SELECT * FROM `slider`",dbCon.connection);
            dataReader = com.ExecuteReader();
            while(dataReader.Read())
            {
                obj = new SliderModel();
                obj.id = int.Parse(dataReader[0].ToString());
                obj.title = dataReader[1].ToString();
                obj.status = dataReader[2].ToString();
                obj.img = dataReader[3].ToString()+","+Convert.ToBase64String((byte[])dataReader[4]);
                DateTime dateTime = Convert.ToDateTime(dataReader[5].ToString());
                obj.date = dateTime.ToString("MM-dd-yyyy");
                if(string.IsNullOrEmpty(dataReader[6].ToString()))
                {
                    obj.lastDate = "Null";
                }
                else
                {
                     DateTime lastDate = Convert.ToDateTime(dataReader[6].ToString());
                    obj.lastDate = lastDate.ToString("MM-dd-yyyy");
                }
               
                listSlider.Add(obj);
            }
            return Ok(listSlider);
        }
        //delete slide
        public IActionResult deleteRow(int get_id)
        {
            dbCon = new dbConnection();
            com = new MySqlCommand("DELETE FROM `slider` WHERE `id`="+get_id,dbCon.connection);
            com.ExecuteNonQuery();   
            return Ok(1);
        }
        //show record
        public IActionResult showRecord(int get_id)
        {
            dbCon = new dbConnection();
            com = new MySqlCommand("SELECT * FROM `slider` WHERE id="+get_id,dbCon.connection);
            dataReader = com.ExecuteReader();
            while(dataReader.Read())
            {
                obj = new SliderModel();
                obj.id = Convert.ToInt32(dataReader[0].ToString());
                obj.title = dataReader[1].ToString();
                obj.status = dataReader[2].ToString();
                obj.img = dataReader[3].ToString()+","+Convert.ToBase64String((byte[])dataReader[4]);
            }
            return Ok(obj);
        }

        //update
        public IActionResult updateRow(SliderModel get_obj,int get_id)
        {
            dbCon = new dbConnection();
            imgbyte = Convert.FromBase64String(get_obj.img);
            string MySqlQuery = "UPDATE slider SET title=@title , status=@status, imgType=@imgType, img=@img, date_edit=@date WHERE id=@id";
            com = new MySqlCommand(MySqlQuery,dbCon.connection);  
            com.Parameters.AddWithValue("id",get_id);
            com.Parameters.AddWithValue("title",get_obj.title);
            com.Parameters.AddWithValue("status",get_obj.status);
            com.Parameters.AddWithValue("imgType",get_obj.imgType);
            com.Parameters.AddWithValue("img",imgbyte);
            com.Parameters.AddWithValue("date",DateTime.Now);
            com.ExecuteNonQuery();
            return Ok(1);
        }
    }
}