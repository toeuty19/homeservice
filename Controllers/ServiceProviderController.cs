using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;

namespace HomeService.Controllers
{
    public class ServiceProviderController:Controller
    {
        private MySqlCommand com {get;set;}
        private MySqlDataReader reader{get;set;}
        private MySqlDataAdapter dataAdapter{get;set;}
        private dbConnection con {get;set;}
        private ServiceProvider provider {get;set;}
        public IActionResult ServiceProviderDashbaord()
        {
            return View();
        }
        public IActionResult ServiceProviderProfile()
        {
            //GetProvider();
            return View();
        }

        public IActionResult EditService()
        {
            return View();
        }
        //SELECT serviceprovider.imgType,serviceprovider.img,serviceprovider.about,serviceprovider.city,servicecategories.name,serviceprovider.loaction,serviceprovider.userId FROM `serviceprovider`,servicecategories WHERE serviceprovider.cateId = servicecategories.id AND serviceprovider.userId = "+userId
        public IActionResult GetProvider()
        {
            try
            {
                int userId = Convert.ToInt16(HttpContext.Session.GetString("userId"));
                con = new dbConnection();
                string sqlQuery = "SELECT serviceprovider.imgType,serviceprovider.img,serviceprovider.about,serviceprovider.city,servicecategories.name,serviceprovider.loaction,serviceprovider.userId,servicecategories.id FROM `serviceprovider`,servicecategories WHERE serviceprovider.cateId = servicecategories.id AND serviceprovider.userId = "+userId;
                dataAdapter = new MySqlDataAdapter(sqlQuery,con.connection);
                DataTable data = new DataTable();
                dataAdapter.Fill(data);
                provider = new ServiceProvider();
                DataRow row = data.Rows[0];
                provider.img = row[0].ToString()+","+Convert.ToBase64String((byte[])row[1]);
                provider.about = row[2].ToString();
                provider.city = row[3].ToString();
                provider.cateName = row[4].ToString();
                provider.location = row[5].ToString();
                provider.serId = int.Parse(row[6].ToString());
                provider.cateId = int.Parse(row[7].ToString());
                
            }
            catch(Exception ex){throw(ex);}
            return Ok(provider);
        }
        private ServiceCategroy categroy{get;set;}
        private List<ServiceCategroy> listCategories{get;set;}
        public IActionResult getServiceCategories()
        {
            listCategories = new List<ServiceCategroy>();
            con  = new dbConnection();
            com = new MySqlCommand("SELECT * FROM `servicecategories`",con.connection);
            reader = com.ExecuteReader();
            while(reader.Read())
            {
                categroy = new ServiceCategroy();  
                categroy.id = int.Parse(reader[0].ToString());
                categroy.name = reader[1].ToString();
                listCategories.Add(categroy);
            }
            return Ok(listCategories);
        }

        public IActionResult EdtitServiceProvider(ServiceProvider GetObj)
        {
            int userId = Convert.ToInt16(HttpContext.Session.GetString("userId"));
            byte[] img = Convert.FromBase64String(GetObj.img);
            string sqlInsert="";
            if(GetObj.serId!=0)
                sqlInsert = "UPDATE `serviceprovider` SET `imgType`=@imgType, `img`=@img, `about`=@about, `city`=@city, `cateId`=@cateId, `loaction`=@location WHERE `userId`=@userId";
            else 
                sqlInsert = "INSERT INTO `serviceprovider`(`userId`, `imgType`, `img`, `about`, `city`, `cateId`, `loaction`) VALUES (@userId,@imgType, @img, @about, @city, @cateId, @location)";
            con = new dbConnection();
            com = new MySqlCommand(sqlInsert,con.connection);
            com.Parameters.AddWithValue("userId",userId);
            com.Parameters.AddWithValue("imgType",GetObj.imgType);
            com.Parameters.AddWithValue("img",img);
            com.Parameters.AddWithValue("about",GetObj.about);
            com.Parameters.AddWithValue("city",GetObj.city);
            com.Parameters.AddWithValue("cateId",GetObj.cateId);
            com.Parameters.AddWithValue("location",GetObj.location); 
            com.ExecuteNonQuery();
            return Ok(1);
        }

    }
}