
using System;
using System.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;

namespace HomeService.Controllers
{
    public class UserController:Controller
    {
        dbConnection db = new dbConnection();
        private MySqlCommand com{get;set;}
        private MySqlDataAdapter data{get;set;}
        private DataTable table{get;set;}
        private string query{get;set;}
        public IActionResult ViewRegister()
        {
            return View();
        }
        public IActionResult ViewLogin()
        {
            return View();
        }
        public IActionResult UserLogin(string email,string pass)
        {
            int result=0;
            data = new MySqlDataAdapter("SELECT name,email,userType,phone,id FROM user WHERE email ='"+email+"' AND password='"+pass+"'",db.connection);
            table = new DataTable();
            data.Fill(table);
            if(table.Rows.Count==1)
            {
                DataRow row = table.Rows[0];
                HttpContext.Session.SetString("name",row[0].ToString());
                HttpContext.Session.SetString("email",row[1].ToString());
                HttpContext.Session.SetString("userType",row[2].ToString());
                HttpContext.Session.SetString("phone",row[3].ToString());
                HttpContext.Session.SetString("userId",row[4].ToString());
                result = 1;
            }
            return Ok(result);
        }
        public IActionResult UserLogout()
        {
            HttpContext.Session.SetString("name","");
            HttpContext.Session.SetString("email","");
            HttpContext.Session.SetString("userType","");
            HttpContext.Session.SetString("phone","");
            HttpContext.Session.SetString("userId","");
            return RedirectToAction("ViewLogin","User");
            
        }
        
        public IActionResult Register(UserModel setObj)
        {
            int result;
            try
            {
                result = Modify(setObj,"INSERT INTO user( name, email, phone, password, userType) VALUES (@name, @email, @phone, @pass, @utype)");
            }
            catch(Exception ex)
            {
                result=ex.HResult;
            }
            return Ok(result);
        }

        private int Modify(UserModel user,string _query)
        {
            com = new MySqlCommand(_query,db.connection);
            com.Parameters.AddWithValue("name",user.name);
            com.Parameters.AddWithValue("email",user.email);
            com.Parameters.AddWithValue("phone",user.phone);
            com.Parameters.AddWithValue("pass",user.password);
            com.Parameters.AddWithValue("utype",user.userType);
            int result = com.ExecuteNonQuery();
            db.connection.Close();
            return result;
        }
    }
}