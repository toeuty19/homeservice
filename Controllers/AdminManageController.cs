using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.AspNetCore.Mvc;

namespace HomeService.Controllers
{
    public class AdminManageController:Controller
    {
        ///dbConnection db = new dbConnection();
        ServiceCategroy service{get;set;}
        private string query{get;set;}
        private List<ServiceCategroy> listService{get;set;}
        private DataTable dTable = new DataTable();
        private int result=0;
        public IActionResult ViewService()
        {
            return View();
        }
        public IActionResult AddServiceCategory()
        {
            return View();
        }
        public IActionResult AllService()
        {
            return View();
        }
        public IActionResult Contact()
        {
            return View();
        }
        public IActionResult ServiceProvider()
        {
            return View();
        }
        //Add New Service Category
        public IActionResult AddNewServiceCategory(ServiceCategroy setData)
        {
            query="INSERT INTO servicecategories(name,slug,imgType,image,feather) VALUES(@name,@slug,@imgType,@img,@feather)";
            service = new ServiceCategroy();
            result = service.Modify(setData,query);
            return Ok(result);
        }
        //show all row on datatable
        public IActionResult GetServieFromDataBase()
        {
            service = new ServiceCategroy();
            listService = new List<ServiceCategroy>();
            query="SELECT * FROM servicecategories";
            dTable = service.ShowRows(query);
            foreach(DataRow row in dTable.Rows)
            {
                service = new ServiceCategroy();
                service.id = int.Parse(row[0].ToString());
                service.name = row[1].ToString();
                service.slug = row[2].ToString();
                service.img = row[3].ToString() +","+Convert.ToBase64String((byte[])row[4]);
                listService.Add(service);
            }
            return Ok(listService);
        }
        //Show row to form
        public IActionResult showRow(int setValue)
        {
            listService = new List<ServiceCategroy>();
            service = new ServiceCategroy();
            DataRow row = service.ShowRows("SELECT * FROM servicecategories WHERE id="+setValue).Rows[0];
            service.name=row[1].ToString();
            service.slug=row[2].ToString();
            service.img = row[3].ToString() +","+Convert.ToBase64String((byte[])row[4]);
            service.feather = row[5].ToString();
            listService.Add(service);
            return Ok(listService);
        }
        //upadate valuse
        public IActionResult updateRow(ServiceCategroy setData)
        {
            query="UPDATE servicecategories SET name=@name,slug=@slug,imgType=@imgType,image=@img,feather=@feather WHERE id="+setData.id;
            service = new ServiceCategroy();
            result = service.Modify(setData,query);
            return Ok(result);
        }
        //delete 
        public IActionResult DeletedRow(int delId)
        {
            service = new ServiceCategroy();
            result = service.DeleteService("DELETE FROM servicecategories WHERE id=@id",delId);
            return Ok(result);
        }
        //Fetch Category
        public IActionResult Categories()
        {
            service = new ServiceCategroy();
            listService = new List<ServiceCategroy>();
            query = "SELECT id,name FROM servicecategories";
            foreach(DataRow row in service.ShowRows(query).Rows)
            {
                service = new ServiceCategroy();
                service.id = int.Parse(row[0].ToString());
                service.name = row[1].ToString();
                listService.Add(service);
            }
            return Ok(listService);
        }

////////////////////////////////////////////////////////////////////////////////
////////////////////////////service section////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

        //Add new Service
        service objService{get;set;}
        List<service> ObjServiceList{get;set;}
        public IActionResult AddService(service getObj )
        {
            objService = new service();
            query = "INSERT INTO service_tbl(name, slug, tagline, service_cate_id, price,discount,discount_Type, imgType, img, description, inclusion, exclusion) VALUES(@name,@slug,@tage,@cid,@price,@discount,@disType,@imgType,@img,@des,@inc,@exc)";
            result = objService.ModifyService(getObj,query);
            return Ok(result);
        }
        //Show all row on view 
        public IActionResult ShowAllService()
        {
            service = new ServiceCategroy();
            ObjServiceList = new List<service>();
            query = "SELECT service_tbl.name,service_tbl.price,service_tbl.status,servicecategories.name,service_tbl.imgType,service_tbl.img,service_tbl.id FROM `service_tbl`,servicecategories WHERE service_tbl.service_cate_id=servicecategories.id";
            foreach(DataRow row in service.ShowRows(query).Rows)
            {
                objService = new service();
                objService.name = row[0].ToString();
                objService.price = decimal.Parse(row[1].ToString());
                objService.status = row[2].ToString();
                objService.categoryName = row[3].ToString();
                objService.img = row[4].ToString()+","+Convert.ToBase64String((byte[])row[5]);
                objService.id = int.Parse(row[6].ToString());
                ObjServiceList.Add(objService);
            }
            return Ok(ObjServiceList);
        }
        //delete service 
        public IActionResult DeleteService(service did)
        {
            //code here
            objService = new service();
            query = "DELETE FROM service_tbl WHERE id=@id";
            result = objService.ModifyService(did,query);
            return Ok(result);
        }
        //show single row 
        public IActionResult ShowSingRow(int getId)
        {
            service = new ServiceCategroy();
            query="SELECT name,slug,tagline,service_cate_id,price,imgType,img,description,inclusion,exclusion,status,discount,discount_Type FROM service_tbl WHERE id="+getId;
            DataRow row = service.ShowRows(query).Rows[0];
            objService = new service();
            objService.name = row[0].ToString();
            objService.slug = row[1].ToString();
            objService.tageline = row[2].ToString();
            objService.category = int.Parse(row[3].ToString());
            objService.price = decimal.Parse(row[4].ToString());
            objService.img = row[5].ToString()+","+Convert.ToBase64String((byte[])row[6]);
            objService.description = row[7].ToString();
            objService.inclusion = row[8].ToString();
            objService.exclusion = row[9].ToString();
            objService.status = row[10].ToString();
            objService.discount = decimal.Parse(row[11].ToString());
            objService.dis_Type = row[12].ToString();
            return Ok(objService);
        }

        //upadate service
        public IActionResult updateService(service getObj)
        {
            objService = new service();
            query = "UPDATE service_tbl SET name=@name, slug=@slug, tagline=@tage, service_cate_id=@cid, price=@price,discount=@discount,discount_Type=@disType, imgType=@imgType, img=@img, description=@des, inclusion=@inc, exclusion=@exc,status='"+getObj.status+"' WHERE id="+getObj.id;
            result = objService.ModifyService(getObj,query);
            return Ok(result);
        }
        //show all service provider user
        public IActionResult GetAllProvider()
        {
            List<ServiceProvider> listProvider = new List<ServiceProvider>();
            service = new ServiceCategroy();
            query = "SELECT s.id, s.imgType,s.img,u.name,u.email,u.phone,s.city,c.name,s.`loaction` FROM `serviceprovider` AS s,user AS u,servicecategories AS c WHERE s.userId = u.id AND s.cateId = c.id ORDER BY s.id DESC";
            DataTable table = new DataTable();
            table = service.ShowRows(query);
            foreach(DataRow row in table.Rows)
            {
                ServiceProvider provider = new ServiceProvider();
                provider.id = int.Parse(row[0].ToString());
                provider.img = row[1].ToString()+","+Convert.ToBase64String((byte[])row[2]);
                provider.name = row[3].ToString();
                provider.email = row[4].ToString();
                provider.phone = row[5].ToString();
                provider.city = row[6].ToString();
                provider.cateName = row[7].ToString();
                provider.location = row[8].ToString();
                listProvider.Add(provider);
            }
            return Ok(listProvider);
        }

    }
}