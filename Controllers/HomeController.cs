﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using HomeService.Models;
using System.Data;
using MySql.Data.MySqlClient;

namespace HomeService.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        
        public IActionResult Index()
        {
            ViewBag.Categories = HomeCategory();
            ViewBag.HomeService = ListOfServiceHomePage();
            ViewBag.CategoriesbyFeather = ListCategories();
            ViewBag.service8Home = ListOfService8();
            ViewBag.Slider = Slider();
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult About()
        {
            return View();
        }
        public IActionResult Faq()
        {
            return View();
        }
        public IActionResult TermsOfUser()
        {
            return View();
        }
        ServiceCategroy service;
        List<ServiceCategroy> listService ;
        public string query{get;set;}
        private List<ServiceCategroy> HomeCategory()
        {
            service = new ServiceCategroy();
            listService = new List<ServiceCategroy>();
            query="SELECT * FROM servicecategories";
            foreach(DataRow row in service.ShowRows(query).Rows)
            {
                service = new ServiceCategroy();
                service.id = int.Parse(row[0].ToString());
                service.name = row[1].ToString();
                service.slug = row[2].ToString();
                service.img = row[3].ToString() +","+Convert.ToBase64String((byte[])row[4]);
                listService.Add(service);
            }
            return listService;
        }
        private service serviceObj ;
        private List<service> listServiceObj;
        private List<service> ListOfServiceHomePage()
        {
            listServiceObj = new List<service>();
            query="SELECT id, name,tagline,price,imgType,img FROM service_tbl WHERE status='yes'";
            foreach(DataRow row in service.ShowRows(query).Rows)
            {
                serviceObj = new service();
                serviceObj.id = int.Parse(row[0].ToString());
                serviceObj.name = row[1].ToString();
                serviceObj.tageline = row[2].ToString();
                serviceObj.price = decimal.Parse(row[3].ToString());
                serviceObj.img = row[4].ToString() +","+Convert.ToBase64String((byte[])row[5]);
                listServiceObj.Add(serviceObj);
            }
            return listServiceObj;
        }
        private List<service> ListOfService8()
        {
            listServiceObj = new List<service>();
            query="SELECT id, name,tagline,price,imgType,img FROM service_tbl WHERE status='yes' LIMIT 8";
            foreach(DataRow row in service.ShowRows(query).Rows)
            {
                serviceObj = new service();
                serviceObj.id = int.Parse(row[0].ToString());
                serviceObj.name = row[1].ToString();
                serviceObj.tageline = row[2].ToString();
                serviceObj.price = decimal.Parse(row[3].ToString());
                serviceObj.img = row[4].ToString() +","+Convert.ToBase64String((byte[])row[5]);
                listServiceObj.Add(serviceObj);
            }
            return listServiceObj;
        }

        private List<ServiceCategroy> ListCategories()
        {
            service = new ServiceCategroy();
            listService = new List<ServiceCategroy>();
            foreach(DataRow row in service.ShowRows("SELECT * FROM servicecategories WHERE feather='Yes' LIMIT 8").Rows)
            {
                service = new ServiceCategroy();
                service.id = int.Parse(row[0].ToString());
                service.name = row[1].ToString();
                service.img = row[3].ToString() +","+Convert.ToBase64String((byte[])row[4]);
                listService.Add(service);
            }

            return listService;
        }

        public IActionResult AutoComplete(string search)
        {
            listServiceObj = new List<service>();
            dbConnection con = new dbConnection();
            query = "SELECT id,name FROM `service_tbl` WHERE name LIKE '%"+search+"%'";
            MySqlDataAdapter data = new MySqlDataAdapter(query,con.connection);
            DataTable dt  = new DataTable();
            data.Fill(dt);
            foreach (DataRow row in dt.Rows)
            {
                listServiceObj.Add(new service{id=int.Parse(row[0].ToString()),name=row[1].ToString()});
            }
            return Ok(listServiceObj);
        }
        //slider
        private SliderModel slider{get;set;}
        private List<SliderModel> listSlider = new List<SliderModel>();
        public List<SliderModel> Slider()
        {
            service = new ServiceCategroy();
            DataTable dt = new DataTable();
            dt = service.ShowRows("SELECT `title`,`imgType`,`img` FROM `slider` WHERE `status`='active'");
            foreach(DataRow row in dt.Rows)
            {
                slider = new SliderModel();
                slider.title = row[0].ToString();
                slider.img = row[1].ToString()+","+Convert.ToBase64String((byte[])row[2]);
                listSlider.Add(slider);

            }
            return listSlider;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
