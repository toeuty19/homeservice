using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.AspNetCore.Mvc;

namespace HomeService.Controllers
{
    public class ServiceController:Controller
    {
        //dbConnection db = new dbConnection();
        private string query{get;set;}
        List<ServiceCategroy> serviceList = new List<ServiceCategroy>();
        ServiceCategroy service ;
        service objservice ;
        List<service> listService = new List<service>();
        public IActionResult ServiceCategory()
        {
            ServiceCategroy();
            return View();
        }
        
        [Route("ServiecByCategories/{id}")]
        public IActionResult ServiecByCategories(int id)
        {
            ServiceByCategory(id);
            return View();
        }
        public IActionResult ServiceCategroy()
        {
            query = "SELECT * FROM servicecategories";
            service = new ServiceCategroy();
            foreach(DataRow row in service.ShowRows(query).Rows)
            {
                service = new ServiceCategroy();
                service.id = int.Parse(row[0].ToString());
                service.name = row[1].ToString();
                service.slug = row[2].ToString();
                service.img = row[3].ToString()+","+Convert.ToBase64String((byte[])row[4]);
                serviceList.Add(service);
            }
            return View(serviceList);
        }
        public IActionResult ServiceByCategory(int cate_Id)
        {
            service = new ServiceCategroy();
            query = "SELECT id, name,tagline,price,imgType,img FROM service_tbl WHERE service_cate_id="+cate_Id;
            foreach(DataRow row in service.ShowRows(query).Rows)
            {
                objservice = new service();
                objservice.id = int.Parse(row[0].ToString());
                objservice.name = row[1].ToString();
                objservice.tageline = row[2].ToString();
                objservice.price = decimal.Parse(row[3].ToString());
                objservice.img = row[4].ToString()+","+Convert.ToBase64String((byte[])row[5]);
                listService.Add(objservice);
            }
            ViewBag.ListData = listService;

            service = new ServiceCategroy();
            DataRow dr = service.ShowRows("SELECT name FROM servicecategories WHERE id="+cate_Id).Rows[0];
            ViewBag.Category = dr[0].ToString();
            return View();
        }

        //------------------------------------------------------------------------///
        //[HttpPost]
        [Route("serviceDetail")]
        public IActionResult ServiceDetail(string name)
        {
            service = new ServiceCategroy();
            objservice = new service();
            query="SELECT name,slug,tagline,service_cate_id,price,imgType,img,description,inclusion,exclusion,status,discount,discount_Type FROM service_tbl WHERE name='"+name+"'";
            DataRow row = service.ShowRows(query).Rows[0];
            objservice = new service();
            objservice.name = row[0].ToString();
            objservice.slug = row[1].ToString();
            objservice.tageline = row[2].ToString();
            objservice.category = int.Parse(row[3].ToString());
            objservice.price = decimal.Parse(row[4].ToString());
            objservice.img = row[5].ToString()+","+Convert.ToBase64String((byte[])row[6]);
            objservice.description = row[7].ToString();
            objservice.inclusion = row[8].ToString();
            objservice.exclusion = row[9].ToString();
            objservice.status = row[10].ToString();
            objservice.discount = decimal.Parse(row[11].ToString());
            objservice.dis_Type = row[12].ToString();
            ViewBag.Total = objservice.price - objservice.discount;
            ViewBag.obj = objservice;
            
            return View();
        }
        
    }
}